package com.ruoyi.battery.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.battery.domain.BatInfo;
import com.ruoyi.battery.service.IBatInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 电池Controller
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
@Controller
@RequestMapping("/battery/batinfo")
public class BatInfoController extends BaseController
{
    private String prefix = "battery/batinfo";

    @Autowired
    private IBatInfoService batInfoService;

    @RequiresPermissions("battery:batinfo:view")
    @GetMapping()
    public String batinfo()
    {
        return prefix + "/batinfo";
    }

    /**
     * 查询电池列表
     */
    @RequiresPermissions("battery:batinfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BatInfo batInfo)
    {
        startPage();
        List<BatInfo> list = batInfoService.selectBatInfoList(batInfo);
        return getDataTable(list);
    }

    /**
     * 导出电池列表
     */
    @RequiresPermissions("battery:batinfo:export")
    @Log(title = "电池", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BatInfo batInfo)
    {
        List<BatInfo> list = batInfoService.selectBatInfoList(batInfo);
        ExcelUtil<BatInfo> util = new ExcelUtil<BatInfo>(BatInfo.class);
        return util.exportExcel(list, "电池数据");
    }

    /**
     * 新增电池
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存电池
     */
    @RequiresPermissions("battery:batinfo:add")
    @Log(title = "电池", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BatInfo batInfo)
    {
        return toAjax(batInfoService.insertBatInfo(batInfo));
    }

    /**
     * 修改电池
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BatInfo batInfo = batInfoService.selectBatInfoById(id);
        mmap.put("batInfo", batInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存电池
     */
    @RequiresPermissions("battery:batinfo:edit")
    @Log(title = "电池", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BatInfo batInfo)
    {
        return toAjax(batInfoService.updateBatInfo(batInfo));
    }

    /**
     * 删除电池
     */
    @RequiresPermissions("battery:batinfo:remove")
    @Log(title = "电池", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(batInfoService.deleteBatInfoByIds(ids));
    }
}
