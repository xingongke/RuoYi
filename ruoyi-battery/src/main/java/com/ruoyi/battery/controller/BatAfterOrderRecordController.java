package com.ruoyi.battery.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.battery.domain.BatAfterOrderRecord;
import com.ruoyi.battery.service.IBatAfterOrderRecordService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 电池售后维修记录Controller
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
@Controller
@RequestMapping("/battery/record")
public class BatAfterOrderRecordController extends BaseController
{
    private String prefix = "battery/record";

    @Autowired
    private IBatAfterOrderRecordService batAfterOrderRecordService;

    @RequiresPermissions("battery:record:view")
    @GetMapping()
    public String record()
    {
        return prefix + "/record";
    }

    /**
     * 查询电池售后维修记录列表
     */
    @RequiresPermissions("battery:record:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BatAfterOrderRecord batAfterOrderRecord)
    {
        startPage();
        List<BatAfterOrderRecord> list = batAfterOrderRecordService.selectBatAfterOrderRecordList(batAfterOrderRecord);
        return getDataTable(list);
    }

    /**
     * 导出电池售后维修记录列表
     */
    @RequiresPermissions("battery:record:export")
    @Log(title = "电池售后维修记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BatAfterOrderRecord batAfterOrderRecord)
    {
        List<BatAfterOrderRecord> list = batAfterOrderRecordService.selectBatAfterOrderRecordList(batAfterOrderRecord);
        ExcelUtil<BatAfterOrderRecord> util = new ExcelUtil<BatAfterOrderRecord>(BatAfterOrderRecord.class);
        return util.exportExcel(list, "电池售后维修记录数据");
    }

    /**
     * 新增电池售后维修记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存电池售后维修记录
     */
    @RequiresPermissions("battery:record:add")
    @Log(title = "电池售后维修记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BatAfterOrderRecord batAfterOrderRecord)
    {
        return toAjax(batAfterOrderRecordService.insertBatAfterOrderRecord(batAfterOrderRecord));
    }

    /**
     * 修改电池售后维修记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BatAfterOrderRecord batAfterOrderRecord = batAfterOrderRecordService.selectBatAfterOrderRecordById(id);
        mmap.put("batAfterOrderRecord", batAfterOrderRecord);
        return prefix + "/edit";
    }

    /**
     * 修改保存电池售后维修记录
     */
    @RequiresPermissions("battery:record:edit")
    @Log(title = "电池售后维修记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BatAfterOrderRecord batAfterOrderRecord)
    {
        return toAjax(batAfterOrderRecordService.updateBatAfterOrderRecord(batAfterOrderRecord));
    }

    /**
     * 删除电池售后维修记录
     */
    @RequiresPermissions("battery:record:remove")
    @Log(title = "电池售后维修记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(batAfterOrderRecordService.deleteBatAfterOrderRecordByIds(ids));
    }
}
