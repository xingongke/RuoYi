package com.ruoyi.battery.mapper;

import java.util.List;
import com.ruoyi.battery.domain.BatAfterOrderRecord;

/**
 * 电池售后维修记录Mapper接口
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
public interface BatAfterOrderRecordMapper 
{
    /**
     * 查询电池售后维修记录
     * 
     * @param id 电池售后维修记录ID
     * @return 电池售后维修记录
     */
    public BatAfterOrderRecord selectBatAfterOrderRecordById(Long id);

    /**
     * 查询电池售后维修记录列表
     * 
     * @param batAfterOrderRecord 电池售后维修记录
     * @return 电池售后维修记录集合
     */
    public List<BatAfterOrderRecord> selectBatAfterOrderRecordList(BatAfterOrderRecord batAfterOrderRecord);

    /**
     * 新增电池售后维修记录
     * 
     * @param batAfterOrderRecord 电池售后维修记录
     * @return 结果
     */
    public int insertBatAfterOrderRecord(BatAfterOrderRecord batAfterOrderRecord);

    /**
     * 修改电池售后维修记录
     * 
     * @param batAfterOrderRecord 电池售后维修记录
     * @return 结果
     */
    public int updateBatAfterOrderRecord(BatAfterOrderRecord batAfterOrderRecord);

    /**
     * 删除电池售后维修记录
     * 
     * @param id 电池售后维修记录ID
     * @return 结果
     */
    public int deleteBatAfterOrderRecordById(Long id);

    /**
     * 批量删除电池售后维修记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBatAfterOrderRecordByIds(String[] ids);
}
