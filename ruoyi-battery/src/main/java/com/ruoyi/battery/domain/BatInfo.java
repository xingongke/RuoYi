package com.ruoyi.battery.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 电池对象 bat_info
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
public class BatInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    private Long id;

    /** 二维码编号 */
    @Excel(name = "二维码编号")
    private String qrCode;

    /** 电池编号mac */
    @Excel(name = "电池编号mac")
    private String batMac;

    /** 电池价格 */
    @Excel(name = "电池价格")
    private BigDecimal batPrice;

    /** 电池名称 */
    @Excel(name = "电池名称")
    private String batName;

    /** 电池类型 */
    @Excel(name = "电池类型")
    private String batType;

    /** 出售时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出售时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sellTime;

    /** 质保类型，1，3个月 2，6个月 3，一年 */
    @Excel(name = "质保类型，1，3个月 2，6个月 3，一年")
    private Long warrantyType;

    /** 质保到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "质保到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date warrantyTime;

    /** 用户ID，门店店长的id */
    @Excel(name = "用户ID，门店店长的id")
    private Long userId;

    /** 部门ID，门店id */
    @Excel(name = "部门ID，门店id")
    private Long deptId;

    /** 描述 */
    @Excel(name = "描述")
    private String batDesc;

    /** 售出的客户姓名 */
    @Excel(name = "售出的客户姓名")
    private String customerName;

    /** 售出的客户手机号码 */
    @Excel(name = "售出的客户手机号码")
    private String customerMobile;

    /** 售出的客户地址 */
    @Excel(name = "售出的客户地址")
    private String customerAddr;

    /** 售出的客户微信号 */
    @Excel(name = "售出的客户微信号")
    private String customerWx;

    /** 电池状态（0正常 1停用） */
    @Excel(name = "电池状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setQrCode(String qrCode) 
    {
        this.qrCode = qrCode;
    }

    public String getQrCode() 
    {
        return qrCode;
    }
    public void setBatMac(String batMac) 
    {
        this.batMac = batMac;
    }

    public String getBatMac() 
    {
        return batMac;
    }
    public void setBatPrice(BigDecimal batPrice) 
    {
        this.batPrice = batPrice;
    }

    public BigDecimal getBatPrice() 
    {
        return batPrice;
    }
    public void setBatName(String batName) 
    {
        this.batName = batName;
    }

    public String getBatName() 
    {
        return batName;
    }
    public void setBatType(String batType) 
    {
        this.batType = batType;
    }

    public String getBatType() 
    {
        return batType;
    }
    public void setSellTime(Date sellTime) 
    {
        this.sellTime = sellTime;
    }

    public Date getSellTime() 
    {
        return sellTime;
    }
    public void setWarrantyType(Long warrantyType) 
    {
        this.warrantyType = warrantyType;
    }

    public Long getWarrantyType() 
    {
        return warrantyType;
    }
    public void setWarrantyTime(Date warrantyTime) 
    {
        this.warrantyTime = warrantyTime;
    }

    public Date getWarrantyTime() 
    {
        return warrantyTime;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setBatDesc(String batDesc) 
    {
        this.batDesc = batDesc;
    }

    public String getBatDesc() 
    {
        return batDesc;
    }
    public void setCustomerName(String customerName) 
    {
        this.customerName = customerName;
    }

    public String getCustomerName() 
    {
        return customerName;
    }
    public void setCustomerMobile(String customerMobile) 
    {
        this.customerMobile = customerMobile;
    }

    public String getCustomerMobile() 
    {
        return customerMobile;
    }
    public void setCustomerAddr(String customerAddr) 
    {
        this.customerAddr = customerAddr;
    }

    public String getCustomerAddr() 
    {
        return customerAddr;
    }
    public void setCustomerWx(String customerWx) 
    {
        this.customerWx = customerWx;
    }

    public String getCustomerWx() 
    {
        return customerWx;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("qrCode", getQrCode())
            .append("batMac", getBatMac())
            .append("batPrice", getBatPrice())
            .append("batName", getBatName())
            .append("batType", getBatType())
            .append("sellTime", getSellTime())
            .append("warrantyType", getWarrantyType())
            .append("warrantyTime", getWarrantyTime())
            .append("userId", getUserId())
            .append("deptId", getDeptId())
            .append("batDesc", getBatDesc())
            .append("customerName", getCustomerName())
            .append("customerMobile", getCustomerMobile())
            .append("customerAddr", getCustomerAddr())
            .append("customerWx", getCustomerWx())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
