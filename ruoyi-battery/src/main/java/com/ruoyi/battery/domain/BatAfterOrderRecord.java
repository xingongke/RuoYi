package com.ruoyi.battery.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 电池售后维修记录对象 bat_after_order_record
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
public class BatAfterOrderRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 二维码编号 */
    private String qrCode;

    /** 电池编号mac */
    @Excel(name = "电池编号mac")
    private String batMac;

    /** 维修物品名称 */
    @Excel(name = "维修物品名称")
    private String repairName;

    /** 订单价格 */
    @Excel(name = "订单价格")
    private BigDecimal orderPrice;

    /** 故障描述 */
    @Excel(name = "故障描述")
    private String faultDesc;

    /** 质保类型，1，3个月 2，6个月 3，一年 */
    @Excel(name = "质保类型，1，3个月 2，6个月 3，一年")
    private Long warrantyType;

    /** 质保到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "质保到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date warrantyTime;

    /** 用户ID，门店店长的id */
    @Excel(name = "用户ID，门店店长的id")
    private Long userId;

    /** 部门ID，门店id */
    @Excel(name = "部门ID，门店id")
    private Long deptId;

    /** 顾客姓名 */
    @Excel(name = "顾客姓名")
    private String customerName;

    /** 顾客手机号码 */
    @Excel(name = "顾客手机号码")
    private String customerMobile;

    /** 顾客地址 */
    @Excel(name = "顾客地址")
    private String customerAddr;

    /** 顾客微信 */
    @Excel(name = "顾客微信")
    private String customerWx;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setQrCode(String qrCode) 
    {
        this.qrCode = qrCode;
    }

    public String getQrCode() 
    {
        return qrCode;
    }
    public void setBatMac(String batMac) 
    {
        this.batMac = batMac;
    }

    public String getBatMac() 
    {
        return batMac;
    }
    public void setRepairName(String repairName) 
    {
        this.repairName = repairName;
    }

    public String getRepairName() 
    {
        return repairName;
    }
    public void setOrderPrice(BigDecimal orderPrice) 
    {
        this.orderPrice = orderPrice;
    }

    public BigDecimal getOrderPrice() 
    {
        return orderPrice;
    }
    public void setFaultDesc(String faultDesc) 
    {
        this.faultDesc = faultDesc;
    }

    public String getFaultDesc() 
    {
        return faultDesc;
    }
    public void setWarrantyType(Long warrantyType) 
    {
        this.warrantyType = warrantyType;
    }

    public Long getWarrantyType() 
    {
        return warrantyType;
    }
    public void setWarrantyTime(Date warrantyTime) 
    {
        this.warrantyTime = warrantyTime;
    }

    public Date getWarrantyTime() 
    {
        return warrantyTime;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setCustomerName(String customerName) 
    {
        this.customerName = customerName;
    }

    public String getCustomerName() 
    {
        return customerName;
    }
    public void setCustomerMobile(String customerMobile) 
    {
        this.customerMobile = customerMobile;
    }

    public String getCustomerMobile() 
    {
        return customerMobile;
    }
    public void setCustomerAddr(String customerAddr) 
    {
        this.customerAddr = customerAddr;
    }

    public String getCustomerAddr() 
    {
        return customerAddr;
    }
    public void setCustomerWx(String customerWx) 
    {
        this.customerWx = customerWx;
    }

    public String getCustomerWx() 
    {
        return customerWx;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("qrCode", getQrCode())
            .append("batMac", getBatMac())
            .append("repairName", getRepairName())
            .append("orderPrice", getOrderPrice())
            .append("faultDesc", getFaultDesc())
            .append("warrantyType", getWarrantyType())
            .append("warrantyTime", getWarrantyTime())
            .append("userId", getUserId())
            .append("deptId", getDeptId())
            .append("customerName", getCustomerName())
            .append("customerMobile", getCustomerMobile())
            .append("customerAddr", getCustomerAddr())
            .append("customerWx", getCustomerWx())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
