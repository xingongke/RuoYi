package com.ruoyi.battery.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.battery.mapper.BatAfterOrderRecordMapper;
import com.ruoyi.battery.domain.BatAfterOrderRecord;
import com.ruoyi.battery.service.IBatAfterOrderRecordService;
import com.ruoyi.common.core.text.Convert;

/**
 * 电池售后维修记录Service业务层处理
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
@Service
public class BatAfterOrderRecordServiceImpl implements IBatAfterOrderRecordService 
{
    @Autowired
    private BatAfterOrderRecordMapper batAfterOrderRecordMapper;

    /**
     * 查询电池售后维修记录
     * 
     * @param id 电池售后维修记录ID
     * @return 电池售后维修记录
     */
    @Override
    public BatAfterOrderRecord selectBatAfterOrderRecordById(Long id)
    {
        return batAfterOrderRecordMapper.selectBatAfterOrderRecordById(id);
    }

    /**
     * 查询电池售后维修记录列表
     * 
     * @param batAfterOrderRecord 电池售后维修记录
     * @return 电池售后维修记录
     */
    @Override
    public List<BatAfterOrderRecord> selectBatAfterOrderRecordList(BatAfterOrderRecord batAfterOrderRecord)
    {
        return batAfterOrderRecordMapper.selectBatAfterOrderRecordList(batAfterOrderRecord);
    }

    /**
     * 新增电池售后维修记录
     * 
     * @param batAfterOrderRecord 电池售后维修记录
     * @return 结果
     */
    @Override
    public int insertBatAfterOrderRecord(BatAfterOrderRecord batAfterOrderRecord)
    {
        batAfterOrderRecord.setCreateTime(DateUtils.getNowDate());
        return batAfterOrderRecordMapper.insertBatAfterOrderRecord(batAfterOrderRecord);
    }

    /**
     * 修改电池售后维修记录
     * 
     * @param batAfterOrderRecord 电池售后维修记录
     * @return 结果
     */
    @Override
    public int updateBatAfterOrderRecord(BatAfterOrderRecord batAfterOrderRecord)
    {
        batAfterOrderRecord.setUpdateTime(DateUtils.getNowDate());
        return batAfterOrderRecordMapper.updateBatAfterOrderRecord(batAfterOrderRecord);
    }

    /**
     * 删除电池售后维修记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBatAfterOrderRecordByIds(String ids)
    {
        return batAfterOrderRecordMapper.deleteBatAfterOrderRecordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除电池售后维修记录信息
     * 
     * @param id 电池售后维修记录ID
     * @return 结果
     */
    @Override
    public int deleteBatAfterOrderRecordById(Long id)
    {
        return batAfterOrderRecordMapper.deleteBatAfterOrderRecordById(id);
    }
}
