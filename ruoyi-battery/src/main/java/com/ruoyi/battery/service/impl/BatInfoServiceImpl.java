package com.ruoyi.battery.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.battery.mapper.BatInfoMapper;
import com.ruoyi.battery.domain.BatInfo;
import com.ruoyi.battery.service.IBatInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 电池Service业务层处理
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
@Service
public class BatInfoServiceImpl implements IBatInfoService 
{
    @Autowired
    private BatInfoMapper batInfoMapper;

    /**
     * 查询电池
     * 
     * @param id 电池ID
     * @return 电池
     */
    @Override
    public BatInfo selectBatInfoById(Long id)
    {
        return batInfoMapper.selectBatInfoById(id);
    }

    /**
     * 查询电池列表
     * 
     * @param batInfo 电池
     * @return 电池
     */
    @Override
    public List<BatInfo> selectBatInfoList(BatInfo batInfo)
    {
        return batInfoMapper.selectBatInfoList(batInfo);
    }

    /**
     * 新增电池
     * 
     * @param batInfo 电池
     * @return 结果
     */
    @Override
    public int insertBatInfo(BatInfo batInfo)
    {
        batInfo.setCreateTime(DateUtils.getNowDate());
        return batInfoMapper.insertBatInfo(batInfo);
    }

    /**
     * 修改电池
     * 
     * @param batInfo 电池
     * @return 结果
     */
    @Override
    public int updateBatInfo(BatInfo batInfo)
    {
        batInfo.setUpdateTime(DateUtils.getNowDate());
        return batInfoMapper.updateBatInfo(batInfo);
    }

    /**
     * 删除电池对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBatInfoByIds(String ids)
    {
        return batInfoMapper.deleteBatInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除电池信息
     * 
     * @param id 电池ID
     * @return 结果
     */
    @Override
    public int deleteBatInfoById(Long id)
    {
        return batInfoMapper.deleteBatInfoById(id);
    }
}
