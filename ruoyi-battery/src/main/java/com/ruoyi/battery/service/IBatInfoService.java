package com.ruoyi.battery.service;

import java.util.List;
import com.ruoyi.battery.domain.BatInfo;

/**
 * 电池Service接口
 * 
 * @author dingyuanyuan
 * @date 2021-06-23
 */
public interface IBatInfoService 
{
    /**
     * 查询电池
     * 
     * @param id 电池ID
     * @return 电池
     */
    public BatInfo selectBatInfoById(Long id);

    /**
     * 查询电池列表
     * 
     * @param batInfo 电池
     * @return 电池集合
     */
    public List<BatInfo> selectBatInfoList(BatInfo batInfo);

    /**
     * 新增电池
     * 
     * @param batInfo 电池
     * @return 结果
     */
    public int insertBatInfo(BatInfo batInfo);

    /**
     * 修改电池
     * 
     * @param batInfo 电池
     * @return 结果
     */
    public int updateBatInfo(BatInfo batInfo);

    /**
     * 批量删除电池
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBatInfoByIds(String ids);

    /**
     * 删除电池信息
     * 
     * @param id 电池ID
     * @return 结果
     */
    public int deleteBatInfoById(Long id);
}
